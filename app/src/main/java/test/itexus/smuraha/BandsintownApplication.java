package test.itexus.smuraha;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import io.realm.Realm;
import test.itexus.smuraha.injections.component.BandsintownAppComponents;

/**
 * Application class
 */

public class BandsintownApplication extends Application {
    private static BandsintownApplication instance;
    private static BandsintownAppComponents component;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Realm.init(getApplicationContext());

        if (BuildConfig.DEBUG) {
            Stetho.initialize(
                    Stetho.newInitializerBuilder(this)
                            .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                            .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                            .build());
        }

        buildComponentAndInject();
    }

    /**
     * Injectects Application components class
     */
    public static void buildComponentAndInject() {
        component = BandsintownAppComponents.Initializer.init(instance);
    }

    /**
     * Gets application instance
     * @return BandsintownApplication - application instance
     */
    public static BandsintownApplication getInstance() {
        return instance;
    }

    /**
     * Gets application components instance
     * @return
     */
    public static BandsintownAppComponents component() {
        return component;
    }
}
