package test.itexus.smuraha.injections.component;

import javax.inject.Singleton;

import dagger.Component;
import dagger.internal.DaggerCollections;
import test.itexus.smuraha.BandsintownApplication;
import test.itexus.smuraha.data.dao.RealmDao;
import test.itexus.smuraha.data.network.AppErrorHandler;
import test.itexus.smuraha.data.network.AppJsonDeserializer;
import test.itexus.smuraha.injections.NetworkServiceModule;
import test.itexus.smuraha.injections.module.MainModule;
import test.itexus.smuraha.injections.module.NetworkApiModule;
import test.itexus.smuraha.ui.activity.DetailsActivity;
import test.itexus.smuraha.ui.activity.MapActivity;
import test.itexus.smuraha.ui.activity.SearchActivity;

/**
 * Application components
 */

@Singleton
@Component(modules = {MainModule.class,
        NetworkApiModule.class,
        NetworkServiceModule.class,
        RealmDao.class})
public interface BandsintownAppComponents {

    class Initializer {
        public static BandsintownAppComponents init(BandsintownApplication app) {
            return  DaggerBandsintownAppComponents.builder()
                    .mainModule(new MainModule(app))
                    .build();
        }
    }

    void inject(NetworkApiModule networkApiModule);
    void inject(SearchActivity activity);
    void inject(AppErrorHandler handler);
    void inject(AppJsonDeserializer serialize);
    void inject(DetailsActivity activity);
    void inject(MapActivity activity);
    void inject(RealmDao dao);
}
