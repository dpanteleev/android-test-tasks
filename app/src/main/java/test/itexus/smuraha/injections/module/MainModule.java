package test.itexus.smuraha.injections.module;

import android.app.Application;
import android.content.res.Resources;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import test.itexus.smuraha.BandsintownApplication;

/**
 * Main Application Module, provides application instance
 */

@Module
public class MainModule {
    private final BandsintownApplication app;

    public MainModule(BandsintownApplication application) {
        app = application;
    }

    @Provides
    @Singleton
    protected Application provideApplication() {
        return app;
    }

    @Provides
    @Singleton
    protected Resources provideResources() {
        return app.getResources();
    }

}
