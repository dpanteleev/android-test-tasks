package test.itexus.smuraha.injections;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import test.itexus.smuraha.data.service.ArtistsService;
import test.itexus.smuraha.data.service.iService.IArtistsService;

/**
 * Network Service Module.
 */

@Module
public class NetworkServiceModule {
    @Singleton
    @Provides
    IArtistsService provideArtistsService(ArtistsService artistService) {
        return artistService;
    }
}
