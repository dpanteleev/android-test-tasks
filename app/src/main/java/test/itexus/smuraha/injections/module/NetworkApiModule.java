package test.itexus.smuraha.injections.module;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;
import test.itexus.smuraha.BuildConfig;
import test.itexus.smuraha.data.network.AppErrorHandler;
import test.itexus.smuraha.data.network.AppJsonDeserializer;
import test.itexus.smuraha.data.network.AppResponseTypeAdapterFactory;
import test.itexus.smuraha.data.network.GsonUTCDateSerializer;
import test.itexus.smuraha.data.network.observable.BandsintownApi;
import test.itexus.smuraha.model.entries.interfaces.ISerializable;

/**
 * Network Api Module
 */

@Module
public class NetworkApiModule {
    private static final String TAG = "NetworkApiModule";

    public static  final String SERVER_DATE_FORMAT  = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String GET_METHOD          = "GET";
    private static final String ACCEPT_HEADER       = "Accept";
    private static final String CONTENT_TYPE_HEADER = "Content-Type";
    private static final String CONTENT_TYPE_FO_GET_METHOD = "application/json; charset=UTF-8";
    private static final int    TIMEOUT_SEC         = 30;

    /**
     * Prepares request adapter
     * @return RestAdapter
     */
    @Singleton
    @Provides
    RestAdapter provideRestAdapter() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(TIMEOUT_SEC, TimeUnit.SECONDS);
        client.setReadTimeout(TIMEOUT_SEC, TimeUnit.SECONDS);
        client.setWriteTimeout(TIMEOUT_SEC, TimeUnit.SECONDS);

        client.networkInterceptors().add(chain -> {
            String method = chain.request().method();
            String header = chain.request().header(CONTENT_TYPE_HEADER);
            Request request;
            if (!TextUtils.equals(GET_METHOD, method) && TextUtils.isEmpty(header)) {
                request = chain.request().newBuilder()
                        .addHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_FO_GET_METHOD)
                        .build();
            } else {
                request = chain.request();
            }
            request = request.newBuilder().addHeader(ACCEPT_HEADER, "application/json").build();
            String url = chain.request().url().getPath();
            Log.d(TAG, "Request to: " + url);
            return chain.proceed(request);
        });
        return new RestAdapter.Builder()
                .setEndpoint(BuildConfig.ENDPOINT)
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setErrorHandler(new AppErrorHandler())
                .setClient(new OkClient(client))
                .setConverter(new GsonConverter(new GsonBuilder()
                        .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                        .excludeFieldsWithoutExposeAnnotation()
                        .registerTypeAdapter(Date.class, new GsonUTCDateSerializer())
                        .registerTypeAdapter(ISerializable.class, new AppJsonDeserializer())
                        .registerTypeAdapterFactory(new AppResponseTypeAdapterFactory())
                        .create()))
                .build();
    }

    @Provides
    BandsintownApi provideBandsintownApi(RestAdapter adapter) {
        return adapter.create(BandsintownApi.class);
    }

    @Singleton
    @Provides
    Gson provideGson() {
        return new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }
}
