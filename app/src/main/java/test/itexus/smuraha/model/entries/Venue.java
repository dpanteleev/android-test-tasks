package test.itexus.smuraha.model.entries;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import test.itexus.smuraha.model.entries.interfaces.IVenue;

/**
 * Venue model
 */
//TODO move names to DataSet constants
@Parcel(Parcel.Serialization.BEAN)
public class Venue extends RealmObject
                implements IVenue{

    @PrimaryKey
    @SerializedName("internalId")
    private int internalId;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("latitude")
    private float latitude;

    @Expose
    @SerializedName("longitude")
    private float longitude;

    @Expose
    @SerializedName("city")
    private String city;

    @Expose
    @SerializedName("region")
    private String region;

    @Expose
    @SerializedName("country")
    private String country;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getInternalId() {
        return internalId;
    }

    public void setInternalId(int internalId) {
        this.internalId = internalId;
    }
}
