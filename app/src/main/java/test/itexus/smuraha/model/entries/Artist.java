package test.itexus.smuraha.model.entries;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import test.itexus.smuraha.model.entries.interfaces.IArtist;

/**
 * Artist data model
 */
//TODO move names to DataSet constants
public class Artist extends RealmObject
                    implements IArtist {

    @PrimaryKey
    @SerializedName("id")
    int id;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("url")
    private String url;

    @Expose
    @SerializedName("image_url")
    private String image_url;

    @Expose
    @SerializedName("thumb_url")
    private String thumb_url;

    @Expose
    @SerializedName("facebook_page_url")
    private String facebook_page_url;

    @Expose
    @SerializedName("mbid")
    private String mbid;

    @Expose
    @SerializedName("tracker_count")
    private int tracker_count;

    @Expose
    @SerializedName("upcoming_event_count")
    private int upcoming_event_count;


    public String getName() {
        return name != null ? name : "";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getThumb_url() {
        return thumb_url;
    }

    public void setThumb_url(String thumb_url) {
        this.thumb_url = thumb_url;
    }

    public String getFacebook_page_url() {
        return facebook_page_url;
    }

    public void setFacebook_page_url(String facebook_page_url) {
        this.facebook_page_url = facebook_page_url;
    }

    public String getMbid() {
        return mbid;
    }

    public void setMbid(String mbid) {
        this.mbid = mbid;
    }

    public int getTracker_count() {
        return tracker_count;
    }

    public void setTracker_count(int tracker_count) {
        this.tracker_count = tracker_count;
    }

    public int getUpcoming_event_count() {
        return upcoming_event_count;
    }

    public void setUpcoming_event_count(int upcoming_event_count) {
        this.upcoming_event_count = upcoming_event_count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
