package test.itexus.smuraha.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import retrofit.RetrofitError;

/**
 * Error Response
 */

public class ErrorResponse  extends Throwable{
    @Expose
    @SerializedName("error")
    private int httpErrorCode;

    @Expose
    @SerializedName("code")
    private double code;

    @Expose
    @SerializedName("details")
    private String errorMessage;

    private RetrofitError retrofitError;

    public int getHttpErrorCode() {
        return httpErrorCode;
    }

    public double getConcreteCode() {
        return code;
    }

    public void setRetrofitError(RetrofitError retrofitError){
        this.retrofitError = retrofitError;
    }

    @Override
    public String getLocalizedMessage() {
        return errorMessage;
    }

    @Override
    public String getMessage() {
        return errorMessage;
    }

    @Override
    public void printStackTrace() {
        System.out.println(retrofitError.getBody().toString());
        retrofitError.printStackTrace();
    }
}
