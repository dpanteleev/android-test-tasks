package test.itexus.smuraha.model.entries;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import test.itexus.smuraha.model.entries.interfaces.IArtistEvent;

/**
 * ArtistEvent model
 */
//TODO move names to DataSet constants
public class ArtistEvent extends RealmObject
                        implements IArtistEvent {
    @PrimaryKey
    @Expose
    @SerializedName("id")
    private int id;

    @SerializedName("internalId")
    private int internalId;

    @Expose
    @SerializedName("artist_id")
    private long artist_id;

    @Expose
    @SerializedName("url")
    private String url;

    @Expose
    @SerializedName("datetime")
    private Date datetime;

    @Expose
    @SerializedName("venue")
    private Venue venue;

    @Expose
    @SerializedName("offers")
    private RealmList<EventOffers> offers;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(long artist_id) {
        this.artist_id = artist_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public RealmList<EventOffers> getOffers() {
        return offers;
    }

    public void setOffers(RealmList<EventOffers> offers) {
        this.offers = offers;
    }

    @Override
    public String getDay() {
        if (datetime == null) return "";
        return new SimpleDateFormat("dd/MM/yyyy").format(datetime);
    }

    @Override
    public String getTime() {
        if (datetime == null) return "";
        return new SimpleDateFormat("HH:mm").format(datetime);
    }

    public int getInternalId() {
        return internalId;
    }

    public void setInternalId(int internalId) {
        this.internalId = internalId;
    }
}
