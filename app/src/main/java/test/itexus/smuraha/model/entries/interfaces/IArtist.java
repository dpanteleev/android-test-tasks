package test.itexus.smuraha.model.entries.interfaces;

/**
 * IArtist
 */

public interface IArtist {
    String getName();

    String getUrl();

    String getImage_url();

    String getThumb_url();

    String getFacebook_page_url();

    String getMbid();

    int getTracker_count();

    int getUpcoming_event_count();
}
