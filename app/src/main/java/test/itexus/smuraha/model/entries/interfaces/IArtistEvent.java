package test.itexus.smuraha.model.entries.interfaces;

import java.util.Date;

import io.realm.RealmList;
import test.itexus.smuraha.model.entries.EventOffers;
import test.itexus.smuraha.model.entries.Venue;

/**
 * IArtistEvent
 */

public interface IArtistEvent {
    int getId();

    long getArtist_id();

    String getUrl();

    Date getDatetime();

    Venue getVenue();

    RealmList<EventOffers> getOffers();

    String getDay();

    String getTime();

    void setInternalId(int internalId);
}
