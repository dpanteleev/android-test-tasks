package test.itexus.smuraha.model.entries.interfaces;

/**
 * IVenue
 */

public interface IVenue {

    float getLatitude();

    float getLongitude();

    String getCity();

    String getRegion();

    String getCountry();

    int getInternalId();

    void setInternalId(int internalId);
}
