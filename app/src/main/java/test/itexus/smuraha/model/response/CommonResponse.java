package test.itexus.smuraha.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Common Response
 */

public class CommonResponse<Resource, MetaType> {
    public static final String FIELD_RESOURCE = "resource";
    public static final String FIELD_META = "meta";
    @Expose
    @SerializedName(FIELD_RESOURCE)
    private Resource resource;

    @Expose
    @SerializedName(FIELD_META)
    private MetaType meta;

    public Resource getResource() {
        return resource;
    }

    public MetaType getMeta() {
        return meta;
    }
}
