package test.itexus.smuraha.model.entries.interfaces;

/**
 * ISerializable
 */

public interface ISerializable {
    void onAfterDeserialization();
}

