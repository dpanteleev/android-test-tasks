package test.itexus.smuraha.model.entries;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import test.itexus.smuraha.model.entries.interfaces.IEventOffers;

/**
 * EventOffers model
 */
//TODO move names to DataSet constants
public class EventOffers extends RealmObject
        implements IEventOffers{
    @PrimaryKey
    private int id;

    @SerializedName("internalId")
    private int internalId;

    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("url")
    private String url;

    @Expose
    @SerializedName("status")
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getInternalId() {
        return internalId;
    }

    public void setInternalId(int internalId) {
        this.internalId = internalId;
    }
}
