package test.itexus.smuraha.model.entries.interfaces;

/**
 * IEventOffers
 */

public interface IEventOffers {
    int getId();

    void setId(int id);

    String getType();

    String getUrl();

    String getStatus();

    int getInternalId();

    void setInternalId(int internalId);
}
