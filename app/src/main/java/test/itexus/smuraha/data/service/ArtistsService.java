package test.itexus.smuraha.data.service;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import test.itexus.smuraha.data.dao.RealmDao;
import test.itexus.smuraha.data.network.observable.BandsintownApi;
import test.itexus.smuraha.data.service.iService.IArtistsService;
import test.itexus.smuraha.model.entries.Artist;
import test.itexus.smuraha.model.entries.ArtistEvent;

/**
 * Artists Service
 */

public class ArtistsService extends BaseNetworkService implements IArtistsService {

    private BandsintownApi bandsintownApi;

    @Inject
    RealmDao realmDao;

    @Inject
    public ArtistsService(BandsintownApi bandsintownApi) {
        this.bandsintownApi = bandsintownApi;
    }

    @Override
    public Subscription performGetArtist(String artistName,
                                         String appId,
                                         int requestCode,
                                         Action1<Artist> onSuccess,
                                         Action1<Throwable> onError) {
        Observable<Artist> observable = bandsintownApi
                .performGetArtist(artistName, appId)
                .doOnNext(artist -> {
                    realmDao.addArtist(artist);
                })
                .observeOn(AndroidSchedulers.mainThread());
        return persistInMemoryAndSubscribe(observable, requestCode, onSuccess, onError);
    }

    @Override
    public Subscription performGetEvents(String artistName,
                                         String appId,
                                         int requestCode,
                                         Action1<List<ArtistEvent>> onSuccess,
                                         Action1<Throwable> onError) {
        Observable<List<ArtistEvent>> observable = bandsintownApi
                .performGetEvents(artistName, appId)
                .doOnNext(artistEvents ->{
                    realmDao.addEvent(artistEvents, artistName);
                })
                .observeOn(AndroidSchedulers.mainThread());
        return persistInMemoryAndSubscribe(observable, requestCode, onSuccess, onError);
    }
}
