package test.itexus.smuraha.data.network;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import javax.inject.Inject;

import test.itexus.smuraha.BandsintownApplication;
import test.itexus.smuraha.model.entries.interfaces.ISerializable;

/**
 * Json Deserializer
 */

public class AppJsonDeserializer implements JsonDeserializer<ISerializable> {
    @Inject
    Gson gson;

    public AppJsonDeserializer(){
        BandsintownApplication.component().inject(this);
    }

    @Override
    public ISerializable deserialize(JsonElement json,
                                     Type typeOfT,
                                     JsonDeserializationContext context) throws JsonParseException {
        ISerializable iSerializable = gson.fromJson(json, typeOfT);
        iSerializable.onAfterDeserialization();
        return iSerializable;
    }
}