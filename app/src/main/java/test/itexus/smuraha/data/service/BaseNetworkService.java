package test.itexus.smuraha.data.service;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LruCache;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import test.itexus.smuraha.data.service.iService.IBaseNetworkService;

/**
 * Base Network Service
 */

public abstract class BaseNetworkService implements IBaseNetworkService {
    private static final int MAX_SIZE = 100;
    @NonNull
    private LruCache<Integer, TypedObservable<?>> cacheObservables = new LruCache<>(MAX_SIZE);

    @Override
    @Nullable
    /** this method retrieves observable from cached observables and then releases memory */
    public <T> Subscription getFromCache(Class<T> clazz,
                                         Integer requestCode,
                                         Action1<T> onNext,
                                         Action1<Throwable> error){
        TypedObservable observable = cacheObservables.get(requestCode);
        if (observable!=null && observable.getClazz().equals(clazz)){
            TypedObservable<T> typedObservable = (TypedObservable<T>)observable;
            cacheObservables.remove(requestCode);
            return typedObservable
                    .getObservable()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(onNext, error);
        }
        return null;
    }

    public <IN> Observable
            .Transformer<IN, Subscription> persistAndSubscribe(Class<IN> inClass,
                                                               int requestCode,
                                                               Action1<IN> actionSuccess,
                                                               Action1<Throwable> actionError) {
        return inObservable -> Observable
                .just(persistInMemoryAndSubscribe(inObservable,
                        requestCode,
                        inClass,
                        actionSuccess,
                        actionError));
    }

    public void removeFromCache(int requestCode){
        cacheObservables.remove(requestCode);
    }

    protected  <T> Subscription subscribe (Observable<T> observable,
                                           int requestCode,
                                           Action1<T> actionSuccess,
                                           Action1<Throwable> actionError){
        return observable.subscribe(
                o->{ removeFromCache(requestCode); actionSuccess.call(o);},
                o->{ removeFromCache(requestCode); o.printStackTrace(); actionError.call(o);}
        );
    }

    protected <T> Subscription persistInMemoryAndSubscribe(Observable<T> observable,
                                                           Integer requestCode,
                                                           Class<T> clazz,
                                                           Action1<T> actionSuccess,
                                                           Action1<Throwable> actionError){
        return subscribe(persistInMemory(observable.cache(), requestCode, clazz),
                requestCode,
                actionSuccess,
                actionError);
    }

    protected <T> Subscription persistInMemoryAndSubscribe(Observable<T> observable,
                                                           Integer requestCode,
                                                           Action1<T> actionSuccess,
                                                           Action1<Throwable> actionError){
        return subscribe(persistInMemory(observable.cache(), requestCode), requestCode, actionSuccess, actionError);
    }

    protected <T> Observable<T> persistInMemory(Observable<T> observable, Integer requestCode){
        cacheObservables.put(requestCode, new TypedObservable<>(observable, observable.getClass()));
        return observable;
    }

    protected <T> Observable<T> persistInMemory(Observable<T> observable,
                                                Integer requestCode,
                                                Class<T> clazz){
        cacheObservables.put(requestCode, new TypedObservable<>(observable, clazz));
        return observable;
    }

    class TypedObservable<T> {
        private Observable<T> observable;
        private Class clazz;
        TypedObservable(Observable<T> observable, Class clazz){
            this.observable = observable;
            this.clazz = clazz;
        }

        public Observable<T> getObservable() {
            return observable;
        }

        public Class getClazz() {
            return clazz;
        }
    }

}
