package test.itexus.smuraha.data.service.iService;

import android.support.annotation.Nullable;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Base Network Service
 */

public interface IBaseNetworkService {
    @Nullable
    <T> Subscription getFromCache(Class<T> clazz,
                                  Integer requestCode,
                                  Action1<T> onNext,
                                  Action1<Throwable> error);

    <IN> Observable.Transformer<IN, Subscription> persistAndSubscribe(Class<IN> inClass, int requestCode,
                                                                      Action1<IN> actionSuccess,
                                                                      Action1<Throwable> actionError);
}
