package test.itexus.smuraha.data.service.iService;

import java.util.List;

import rx.Subscription;
import rx.functions.Action1;
import test.itexus.smuraha.model.entries.Artist;
import test.itexus.smuraha.model.entries.ArtistEvent;

/**
 * Artists Service Interface
 */

public interface IArtistsService {
    Subscription performGetArtist(String artistName,
                                         String appId,
                                         int requestCode,
                                         Action1<Artist> onSuccess,
                                         Action1<Throwable> onError);

    Subscription performGetEvents(String artistName,
                                         String appId,
                                         int requestCode,
                                         Action1<List<ArtistEvent>> onSuccess,
                                         Action1<Throwable> onError);
}
