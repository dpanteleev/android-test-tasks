package test.itexus.smuraha.data.dao;

/**
 * DataBase Constants Set
 */

//TODO moves all DataBase constants here
public class DataSet {
    private DataSet() {}

    public static class BaseColumns {
            private BaseColumns() {}
            public static final String COLUMN_ID = "id";
            public static final String COLUMN_INTERNAL_ID = "internalId";
        }

    public static class ArtistColumns extends BaseColumns{
        private ArtistColumns() {}
        public static final String COLUMN_NAME = "name";
    }

    public static class ArtistEventColumns extends BaseColumns{
        private ArtistEventColumns() {}
    }
}
