package test.itexus.smuraha.data.dao;

import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Case;
import io.realm.Realm;
import test.itexus.smuraha.model.entries.Artist;
import test.itexus.smuraha.model.entries.ArtistEvent;
import test.itexus.smuraha.model.entries.EventOffers;

import static test.itexus.smuraha.data.dao.DataSet.BaseColumns.COLUMN_ID;
import static test.itexus.smuraha.data.dao.DataSet.BaseColumns.COLUMN_INTERNAL_ID;
import static test.itexus.smuraha.data.dao.DataSet.ArtistColumns.COLUMN_NAME;

/**
 * DataBase Manager
 */

@Module
public class RealmDao {
    Realm realm;

    @Singleton
    @Provides
    public RealmDao provideRealmDao() {
        return new RealmDao();
    }

    /**
     * Retrieves artist from DataBase by Name/contains/INSENSITIVE
     * Gets first record satisfy the criteria
     * @param artistName - string
     * @return Artist object
     */
    public Artist getArtist(String artistName){
        realm = Realm.getDefaultInstance();
        return realm.where(Artist.class)
                    .contains(COLUMN_NAME,artistName, Case.INSENSITIVE)
                    .findFirst();
    }

    /**
     * Retrieves artist Events from DataBase by Name/contains/INSENSITIVE
     * @param artistName - string
     * @return - list of events
     */
    public List<ArtistEvent> getEvents(String artistName){
        realm = Realm.getDefaultInstance();
        try {
            int artistInternalId = getArtist(artistName).getId();
            return realm.where(ArtistEvent.class)
                    .equalTo(COLUMN_INTERNAL_ID, artistInternalId)
                    .findAll();
        }catch (NullPointerException ex){
            return null;
        }
    }

    /**
     * Saves/updates Artist to DataBase
     * @param artist
     */
    public void addArtist(Artist artist){
        realm = Realm.getDefaultInstance();
        if (artist == null) return;
        int key = -1;
        try {
             key = realm.where(Artist.class)
                     .equalTo(COLUMN_NAME,artist.getName())
                     .findFirst()
                    .getId();
       } catch(Exception ex) {}
        if (key == -1) key = getNextKey(Artist.class);

        artist.setId(key);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(artist);
        realm.commitTransaction();
    }

    /**
     * Saves/updates Artist Events to DataBase
     * @param events
     * @param artistName
     */
    public void addEvent(List<ArtistEvent> events, String artistName){
        realm = Realm.getDefaultInstance();
        int key = -1;
        int eventKey;
        try {
            key = realm.where(Artist.class)
                    .equalTo(COLUMN_NAME,artistName)
                    .findFirst()
                    .getId();

            realm.where(EventOffers.class)
                    .equalTo(COLUMN_INTERNAL_ID,key)
                    .findAll().deleteAllFromRealm();
            eventKey = getNextKey(EventOffers.class);

            for(ArtistEvent event:events){
                event.setInternalId(key);
                event.getVenue().setInternalId(key);
                for (EventOffers offer: event.getOffers()){
                    offer.setInternalId(key);
                    offer.setId(eventKey++);
                }
            }
        } catch(Exception ex) {
            return;
        }

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(events);
        realm.commitTransaction();
    }

    /**
     * Gets next key value for COLUMN_ID field.
     * used to increment feature
     * @param clazz - table class (realm object)
     * @return - max value + 1
     */
    private int getNextKey(Class clazz){
        int key;
        try {
            key = realm.where(clazz).max(COLUMN_ID).intValue() + 1;
        } catch(Exception ex) {
            key = 0;
        }
        return key;
    }

}
