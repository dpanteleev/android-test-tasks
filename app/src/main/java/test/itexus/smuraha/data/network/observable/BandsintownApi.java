package test.itexus.smuraha.data.network.observable;

import java.util.List;

import retrofit.RetrofitError;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;
import test.itexus.smuraha.model.entries.Artist;
import test.itexus.smuraha.model.entries.ArtistEvent;

/**
 * Retrofit requests interface.
 */

public interface BandsintownApi {
    /**
     * Gets Artist by name request
     * @param artistsName
     * @param appId
     * @return Artist
     * @throws RetrofitError
     */
    @GET("/artists/{artistName}")
    Observable<Artist> performGetArtist(@Path("artistName") String artistsName,
                                         @Query("app_id") String appId)
            throws RetrofitError;

    /**
     * Gets Artist Event by Artist name request
     * @param artistsName
     * @param appId
     * @return List<ArtistEvent>
     * @throws RetrofitError
     */
    @GET("/artists/{artistName}/events")
    Observable<List<ArtistEvent>> performGetEvents(@Path("artistName") String artistsName,
                                                   @Query("app_id") String appId)
            throws RetrofitError;
}