package test.itexus.smuraha.data.network;

import android.app.Application;

import javax.inject.Inject;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;
import test.itexus.smuraha.BandsintownApplication;
import test.itexus.smuraha.R;
import test.itexus.smuraha.model.response.ErrorResponse;

/**
 * Request Error handler.
 */

public class AppErrorHandler implements ErrorHandler {
    private static final String TAG = AppErrorHandler.class.getSimpleName();

    @Inject
    Application ctx;

    public AppErrorHandler() {
        BandsintownApplication.component().inject(this);
    }

    @Override
    public Throwable handleError(RetrofitError cause) {
        String errorDescription;

        errorDescription = cause.getLocalizedMessage();

        if (cause.getKind() == RetrofitError.Kind.NETWORK) {
            errorDescription = ctx.getString(R.string.msg_error_connection_failed);
        } else if (cause.getResponse() != null) { //TODO process "no data found" response
            try {
                    ErrorResponse errorResponse = (ErrorResponse) cause.getBodyAs(ErrorResponse.class);
                    if (errorResponse != null) {
                        errorResponse.setRetrofitError(cause);
                        return errorResponse;
                    }
            } catch (Exception ex) {
                errorDescription = ex.getLocalizedMessage();
            }
        }
        cause.printStackTrace();
        return new Exception(errorDescription);
    }
}
