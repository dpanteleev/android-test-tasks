package test.itexus.smuraha.ui.activity;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.parceler.Parcels;

import test.itexus.smuraha.BandsintownApplication;
import test.itexus.smuraha.R;
import test.itexus.smuraha.model.entries.Venue;

/**
 * Map activity
 * Displace map with marker of event
 */
public class MapActivity extends BaseActivity
        implements OnMapReadyCallback {
    public static final String EXTRA_VENUE = "MapActivity.venue";

    private MapFragment fMap;
    private Venue venue;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_map;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        BandsintownApplication.component().inject(this);
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        venue = Parcels.unwrap(i.getExtras().getParcelable(EXTRA_VENUE));
        fMap = (MapFragment) getFragmentManager().findFragmentById(R.id.fmap);
        fMap.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (venue == null) return;
        LatLng position = new LatLng(venue.getLatitude(),venue.getLongitude());
        googleMap.addMarker(new MarkerOptions()
                .position(position)
                .title(venue.getName()));
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(position));
    }
}

