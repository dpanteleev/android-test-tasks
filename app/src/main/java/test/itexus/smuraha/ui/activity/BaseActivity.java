package test.itexus.smuraha.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import rx.Subscription;
import test.itexus.smuraha.model.entries.interfaces.IArtist;
import test.itexus.smuraha.ui.activity.interfaces.ISubscribeHandler;

/**
 * Basic Activity class
 */

public abstract class BaseActivity extends AppCompatActivity
                                implements ISubscribeHandler{

    private static final int SNACK_BAR_DURATION = 5000;

    private List<Subscription> subscriptions = new ArrayList<>();
    private InputMethodManager inputMethodManager;
    private View rootView;

    /**
     * Defines layout id
     * @return
     */
    public abstract int getLayoutResId();

    /**
     * Returns root activity view
     * @return
     */
    public View getRootView() {
        return rootView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());

        ButterKnife.bind(this);
        inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        rootView = findViewById(android.R.id.content);
    }

    /**
     * Adds subscription to list
     * @param subscription - event subscription
     */
    @Override
    public void bind(Subscription subscription){
        //todo progress view
        subscriptions.add(subscription);
    }

    /**
     * Cancel Activity processes
     */
    @Override
    protected void onStop() {
        super.onStop();
        for (Subscription subscription : subscriptions) {
            unSubscribe(subscription);
        }
        subscriptions.clear();
    }

    /**
     * Remove subscription
     * @param subscription
     */
    @Override
    public void unSubscribe(Subscription subscription) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    /**
     * On error handler
     * @param throwable
     */
    public void onError(Throwable throwable) {
        showSnackBar(throwable.getLocalizedMessage());
        throwable.printStackTrace();
    }

    /**
     * Shows message to user
     * @param text
     */
    public void showSnackBar(String text) {
        showSnackBar(getRootView(), text);
    }

    /**
     * Shows message by ID to user
     * @param resId
     */
    public void showSnackBar(@StringRes int resId) {
        showSnackBar(getRootView(), getString(resId));
    }

    /**
     * Shows message by ID to user
     * @param rootView
     * @param text
     */
    public void showSnackBar(View rootView, String text) {
        hideKeyboard();
        Snackbar snackbar = Snackbar.make(rootView, text, SNACK_BAR_DURATION);
        setUpSnackBarView(snackbar.getView(), snackbar);
        snackbar.show();
    }

    /**
     * Hides keyboard
     */
    protected void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void setUpSnackBarView(View snackBarView, Snackbar snackbar) {
        snackBarView.setOnClickListener(v -> snackbar.dismiss());
        snackBarView.setBackgroundColor(Color.BLACK);
        TextView tv = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
    }
}
