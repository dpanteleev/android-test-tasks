package test.itexus.smuraha.ui.activity.interfaces;

import rx.Subscription;

/**
 * Subscribe Handler
 */

public interface ISubscribeHandler {
    void bind(Subscription subscription);
    void unSubscribe(Subscription subscription);
}