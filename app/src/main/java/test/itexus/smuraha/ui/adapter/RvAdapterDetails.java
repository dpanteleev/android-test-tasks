package test.itexus.smuraha.ui.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import test.itexus.smuraha.R;
import test.itexus.smuraha.model.entries.ArtistEvent;
import test.itexus.smuraha.model.entries.interfaces.IArtistEvent;
import test.itexus.smuraha.ui.ViewFactory;

/**
 * RecyclerView Adapter for Details Activity
 */

public class RvAdapterDetails extends BaseRvAdapter{

    private OnItemClickListener<IArtistEvent> onMapClickListener;
    private OnItemClickListener<IArtistEvent> onOffersClickListener;
    private OnItemClickListener<String> onWebClickListener;

    private RvAdapterDetails(){}

    @Override
    public RvAdapterDetails.EventsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_events_feed, parent, false);
        EventsViewHolder holder = new EventsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder rvHolder, int position) {
        if (rvHolder instanceof EventsViewHolder){
            EventsViewHolder holder = (EventsViewHolder)rvHolder;
            IArtistEvent item = (ArtistEvent) getItem(position);
            holder.tvDate.setText(item.getDay());
            holder.tvTime.setText(item.getTime());
            holder.tvCity.setText(item.getVenue().getCity());
            holder.tvPlace.setText(item.getVenue().getName());
            setUpOnClicksListeners(item, holder, position);
        }
    }

    /**
     * Set up click listeners
     * @param event
     * @param holder
     * @param position
     */
    private void setUpOnClicksListeners(IArtistEvent event,
                                        EventsViewHolder holder,
                                        int position) {
        if (onMapClickListener != null) {
            ViewFactory.setDebounceClickListener(holder.ibMap,
                    view -> onMapClickListener.onItemClicked(position, event));
        }

        if (onOffersClickListener != null) {
            ViewFactory.setDebounceClickListener(holder.ibOffers,
                    view -> onOffersClickListener.onItemClicked(position, event));
        }

        if (onWebClickListener != null) {
            ViewFactory.setDebounceClickListener(holder.ibWeb,
                    view -> onWebClickListener.onItemClicked(position, event.getUrl()));
        }
    }

    /**
     * Handler
     */
    public static class EventsViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvDate)
        TextView tvDate;

        @Bind(R.id.tvTime)
        TextView tvTime;

        @Bind(R.id.tvPlace)
        TextView tvPlace;

        @Bind(R.id.tvCity)
        TextView tvCity;

        @Bind(R.id.ibMap)
        ImageButton ibMap;

        @Bind(R.id.ibOffers)
        ImageButton ibOffers;

        @Bind(R.id.ibWeb)
        ImageButton ibWeb;

        public EventsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    /**
     * Builder
     * @return
     */
    public static Builder newBuilder() {
        return new RvAdapterDetails().new Builder();
    }

    public class Builder {

        protected Builder() {
        }

        /**
         * Set up click on map button
         * @param val
         * @return Builder
         */
        public Builder setOnMapClickListener(OnItemClickListener<IArtistEvent> val) {
            onMapClickListener = val;
            return this;
        }

        /**
         * Set up click on Tickets button
         * @param val
         * @return Builder
         */
        public Builder setOnOffersClickListener(OnItemClickListener<IArtistEvent> val) {
            onOffersClickListener = val;
            return this;
        }

        /**
         * Set up click on GoWeb button
         * @param val
         * @return Builder
         */
        public Builder setOnWebClickListener(OnItemClickListener<String> val) {
            onWebClickListener = val;
            return this;
        }

        /**
         * Build adapter
         * @return RvAdapterDetails
         */
        public RvAdapterDetails build() {
            return RvAdapterDetails.this;
        }
    }
}
