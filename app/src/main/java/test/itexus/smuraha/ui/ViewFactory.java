package test.itexus.smuraha.ui;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import com.jakewharton.rxbinding.view.RxView;

/**
 * View Factory
 */

public class ViewFactory  implements LayoutInflater.Factory {
    public static final int DEBOUNCE_TIME = 100;

    private LayoutInflater.Factory baseFactory;

    public static void setDebounceClickListener(View view, Action1<Void> onClick) {
        RxView.clicks(view)
                .debounce(DEBOUNCE_TIME, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(onClick);
    }

    public ViewFactory(LayoutInflater.Factory baseFactory) {
        this.baseFactory = baseFactory;
    }


    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        if (name.equals(SwipeRefreshLayout.class.getName())) {
            SwipeRefreshLayout swipeRefreshLayout = new SwipeRefreshLayout(context, attrs);
            return swipeRefreshLayout;
        }
        return baseFactory.onCreateView(name, context, attrs);
    }
}