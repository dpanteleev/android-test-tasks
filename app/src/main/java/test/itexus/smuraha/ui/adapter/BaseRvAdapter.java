package test.itexus.smuraha.ui.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Base RecyclerView Adapter
 */

public abstract class BaseRvAdapter<T, VH extends RecyclerView.ViewHolder>
            extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    protected List<T> mObjects;

    public BaseRvAdapter() {
        mObjects = new ArrayList<>();
    }

    /**
     * Adds object to list
     * @param object
     */
    public void add(final T object) {
        mObjects.add(object);
        notifyItemInserted(mObjects.size());
    }

    /**
     * Adds object set to list
     * @param mObjects
     */
    public void addAll(List<T> mObjects) {
        this.mObjects.addAll(mObjects);
        notifyDataSetChanged();
    }

    /**
     * Removes all data
     */
    public void clear() {
        int count = mObjects.size();
        mObjects.clear();
        notifyItemRangeRemoved(0, count);
    }

    @Override
    public int getItemCount() {
        return mObjects.size();
    }

    /**
     * Gets item by id
     */
    public T getItem(final int position) {
        if (position > mObjects.size() - 1) {
            throw new IllegalArgumentException("Invalid position for getItem(position)");
        }
        return mObjects.get(position);
    }

    public long getItemId(final int position) {
        return position;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    /**
     * Click listener interface
     * @param <T>
     */
    public interface OnItemClickListener<T> {
        void onItemClicked(int position, T item);
    }
}
