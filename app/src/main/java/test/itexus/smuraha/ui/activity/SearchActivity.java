package test.itexus.smuraha.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.OnClick;
import test.itexus.smuraha.BandsintownApplication;
import test.itexus.smuraha.R;
import test.itexus.smuraha.data.dao.RealmDao;
import test.itexus.smuraha.data.service.iService.IArtistsService;
import test.itexus.smuraha.model.entries.interfaces.IArtist;

/**
 * Search activity
 */
public class SearchActivity extends BaseActivity {
    private static final int REQUEST_CODE_ARTIST = 100;

    @Inject
    IArtistsService artistsService;

    @Inject
    RealmDao realmDao;

    @Bind(R.id.svSearch)
    SearchView svSearch;

    @Bind(R.id.tvName)
    TextView tvName;

    @Bind(R.id.tvEvents)
    TextView tvEvents;

    @Bind(R.id.btFacebookPage)
    Button btFacebookPage;

    @Bind(R.id.btEventsDetails)
    Button btEventsDetails;

    @Bind(R.id.ivImage)
    ImageView ivImage;

    IArtist artist;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_search;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        BandsintownApplication.component().inject(this);
        super.onCreate(savedInstanceState);

        svSearch.setOnQueryTextListener(onQuerySearch);

        btFacebookPage.setVisibility(View.GONE);
        btEventsDetails.setVisibility(View.GONE);
    }

    /**
     * Handle on success action
     * @param artist
     */
    public void onSuccess(IArtist artist) {
        this.artist = artist;
        fillData();
    }

    /**
     * Fill UI from artist object
     */
    private void fillData(){
        if (artist == null){
            tvName.setText("");
            tvEvents.setText("");
            ivImage.setVisibility(View.INVISIBLE);
            btFacebookPage.setVisibility(View.GONE);
            btEventsDetails.setVisibility(View.GONE);
            return;
        }

        tvName.setText(artist.getName());

        ivImage.setVisibility(View.VISIBLE);
        Glide.with(this)
                .load(artist.getImage_url())
                .into(ivImage);

        tvEvents.setText(getResources()
                .getQuantityString(R.plurals.events_available,
                        artist.getUpcoming_event_count(),
                        artist.getUpcoming_event_count()));

        if (!TextUtils.isEmpty(artist.getFacebook_page_url())){
            btFacebookPage.setVisibility(View.VISIBLE);
        }else{
            btFacebookPage.setVisibility(View.GONE);
        }

        if (artist.getUpcoming_event_count() > 0){
            btEventsDetails.setVisibility(View.VISIBLE);
        }else{
            btEventsDetails.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btEventsDetails)
    void btEventsDetailsClick(){
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.EXTRAS_NAME, artist.getName());
        startActivity(intent);
    }

    @OnClick(R.id.btFacebookPage)
    void btFacebookPageClick(){
        //// TODO: open FB app
        if (artist.getFacebook_page_url() != null){
            Intent intent =  new Intent(Intent.ACTION_VIEW, Uri.parse(artist.getFacebook_page_url()));
            startActivity(intent);
        }else{
            showSnackBar(R.string.msg_error_no_data);
        }
    }

    /**
     * Handle search button action
     */
    private SearchView.OnQueryTextListener onQuerySearch = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextChange(String newText) {
            return true;
        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            if (!TextUtils.isEmpty(query)) {//todo validator
                bind(artistsService.performGetArtist(query,
                        getString(R.string.app_name),
                        REQUEST_CODE_ARTIST,
                        SearchActivity.this::onSuccess,
                        SearchActivity.this::onError));
            }else{
                showSnackBar(R.string.msg_error_empty);
            }
            return true;
        }
    };

    /**
     * Gets data from db in case of error
     * @param throwable
     */
    @Override
    public void onError(Throwable throwable) {
        super.onError(throwable);
        this.artist = realmDao.getArtist(svSearch.getQuery().toString());
        fillData();
    }
}
