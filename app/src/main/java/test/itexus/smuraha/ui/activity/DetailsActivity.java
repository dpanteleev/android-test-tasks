package test.itexus.smuraha.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import test.itexus.smuraha.BandsintownApplication;
import test.itexus.smuraha.R;
import test.itexus.smuraha.data.dao.RealmDao;
import test.itexus.smuraha.data.service.iService.IArtistsService;
import test.itexus.smuraha.model.entries.ArtistEvent;
import test.itexus.smuraha.model.entries.EventOffers;
import test.itexus.smuraha.model.entries.interfaces.IArtistEvent;
import test.itexus.smuraha.ui.adapter.RvAdapterDetails;

/**
 * Event Details Activity
 */
public class DetailsActivity extends BaseActivity {
    public static final String EXTRAS_NAME = "DetailsActivity.name";
    private static final int REQUEST_CODE_ARTIST_DETAIL = 100;

    @Inject
    RealmDao realmDao;

    @Inject
    IArtistsService artistsService;

    @Bind(R.id.rvList)
    RecyclerView rvList;

    List<ArtistEvent> artistEvents;
    RvAdapterDetails  adapterEvents;

    @Override
    public int getLayoutResId() {
        return R.layout.activity_details;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        BandsintownApplication.component().inject(this);
        super.onCreate(savedInstanceState);

        initRvList();

        if (getIntent() != null) {
            String name = getIntent().getExtras().getString(EXTRAS_NAME, "");
            bind(artistsService.performGetEvents(name,
                    getString(R.string.app_name),
                    REQUEST_CODE_ARTIST_DETAIL,
                    this::onSuccess,
                    this::onError));
        }
    }

    /**
     * Init RecyclerView
     */
    private void initRvList(){
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvList.setLayoutManager(llm);
        adapterEvents = RvAdapterDetails.newBuilder()
                .setOnMapClickListener(this::onMapClick)
                .setOnOffersClickListener(this::onOffersClick)
                .setOnWebClickListener(this::onWebClick)
                .build();
        rvList.setAdapter(adapterEvents);
    }

    /**
     * Handle ou success
     * @param artistEvents
     */
    public void onSuccess(List<ArtistEvent> artistEvents) {
        this.artistEvents = artistEvents;
        adapterEvents.addAll(artistEvents);
    }

    /**
     * Handle map button click
     * @param position
     * @param event
     */
    public void onMapClick(int position, IArtistEvent event){
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra(MapActivity.EXTRA_VENUE, Parcels.wrap(event.getVenue()));
        startActivity(intent);
    }

    /**
     * Handle offers button click
     * @param position
     * @param event
     */
    public void onOffersClick(int position, IArtistEvent event){
        if (event.getOffers() == null) return;
        String message = getString(R.string.msg_offers);
        for (EventOffers offer: event.getOffers()) {
            message+="\n"+offer.getType() + " - " + offer.getStatus();
        }
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }

    /**
     * Handle web button click
     * @param position
     * @param event
     */
    public void onWebClick(int position, String event){
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(event));
        startActivity(i);
    }
}
